//WAP to find the sum of n fractions.
#include<stdio.h>

struct Fraction
{
	int n, numerator[100], denominator[100];
}fraction;

int input()
{
	printf("Enter the number of fractions that we need:\n");
	scanf("%d", &fraction.n);

	printf("Enter the value of Numerator:\n");
	for(int i=0; i < fraction.n; i++)
	{
	scanf("%d",&fraction.numerator[i]);
}

printf("Enter the value of Denominator:\n");
for(int i=0; i < fraction.n; i++)
{
	scanf("%d",&fraction.denominator[i]);
}
}


int gcd(int x, int y)
{
	if(y == 0)
	{
	return x;
}

return gcd(y, x % y);
}

int LCM(int array[], int size)
{
	int lcm = array[0];
	for(int i=1; i<size; i++)
	{
	lcm = (((array[i]*lcm)) / (gcd(array[i], lcm)));
}

return lcm;
}

int display(int Num, int Den)
{
	printf("Sum of n fraction are : %d/%d", Num, Den);
}

int sum_nFractions(int limit, int num[], int den[])
{
	int Numerator=0;

	int Denominator = LCM(den, limit);

	for(int i = 0; i < limit; i++)
	{
	Numerator = Numerator + (num[i]) * (Denominator/den[i]);
}

int GCD = gcd (Numerator, Denominator);

Numerator = Numerator/GCD;
Denominator = Denominator/GCD;

display(Numerator, Denominator);
}

int main()
{
  	input();
	sum_nFractions(fraction.n, fraction.numerator, fraction.denominator);
	return 0;
}