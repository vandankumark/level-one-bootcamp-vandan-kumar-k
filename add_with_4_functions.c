//Write a program to add two user input numbers using 4 functions.

#include <stdio.h>

int first_num()
{
    int num1;
    printf("enter the first number: ");
    scanf("%d",&num1);
    return num1;
}

int second_num()
{
    int num2;
    printf("enter the second number: ");
    scanf("%d",&num2);
    return num2;
}

int add_numbers(int first_num,int second_num)
{
    return (first_num + second_num);
}

int main()
{
    int a = first_num();
    int b = second_num();
    int sum = add_numbers(a,b);
    printf("sum of two numbers are %d:",sum);
   return 0;
}
