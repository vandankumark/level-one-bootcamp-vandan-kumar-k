//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>

float variable()
{
    float a;
    scanf("%f",&a);
    return a;
}


float volume(float h, float b, float d)
{
    return ((h * d * b) + ( d / b)) / 3;
}


float output(float h, float b, float d, float volume)
{
    printf("The Volume of the tromboloid when Height %f, Breadth %f and Depth %f is: %2f", h,b,d,volume);
}


int main()
{
    float h,b,d,temp;
    printf("Enter the Height : ");
    h = variable();
    
    printf("Enter the Breadth : ");
    b = variable();
    
    printf("Enter the Depth of the : ");
    d = variable();
	
    temp = volume(h,b,d);
    output(h,b,d,temp);
    
    return 0;
}