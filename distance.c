//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>

float input()
{
 float temp;
 scanf("%f",&temp);
 return temp;
}

 
float distance(float a, float b)
{
 float dist;
 dist = sqrt(a*a + b*b);
 return dist;
}


float result(float a, float b, float output)
{
 printf("the distance between the points %f and %f is %f",a,b,output);
}


float main()
{
 float x,y,x2,y2,x1,y1,output;
 printf("locating the 1st point ");
 printf("\nenter the value of x1: ");
 x1 = input();
 printf("enter the value of y1: ");
 y1 = input();
 
 printf("locating the second point");
 printf("\nenter the value of x2: ");
 x2 = input();
 printf("enter the value of y2: ");
 y2 = input();
 
 x = (x2 - x1);
 y = (y2 - y1);
 
 output = distance(x, y);
 result(x, y, output);
 
 return 0;
 }
 