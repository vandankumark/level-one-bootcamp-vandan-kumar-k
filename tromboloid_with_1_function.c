//Write a program to find the volume of a tromboloid using one function
#include <stdio.h>

int main()
{
     float h,d,b,volume;
     printf("enter the values of h,d,b respectively: ");
     scanf("%f %f %f",&h,&d,&b);
     volume = ((h*d*b)+(d/b))/3;
     printf("volume of tromboloid given %3f,%3f,%3f is %3f",h,d,b,volume);
    return 0;
}
